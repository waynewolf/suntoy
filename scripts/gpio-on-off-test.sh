#!/bin/bash

GPIO_DIRECTION=/sys/class/gpio/gpio296/direction
GPIO_VALUE=/sys/class/gpio/gpio296/value

adb shell "echo out > $GPIO_DIRECTION"

while true
do
  adb shell "echo 1 > $GPIO_VALUE"
  sleep 0.01
  adb shell "echo 0 > $GPIO_VALUE"
  sleep 0.02
done