#!/bin/bash

# export pca9685 channel 0
adb shell test-pca9685-pwm -c 0 -e

while true
do
  adb shell test-pca9685-pwm -c 0 --period 10000000 --duty-cycle 5000000
  sleep 0.1
done