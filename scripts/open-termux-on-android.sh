#!/bin/bash

######################################################################################################################
# refer to https://oliverse.ch/tech/2015/11/06/run-an-ssh-server-on-your-android-with-termux.html                    #
# for how to install termux and sshd on android                                                                      #
######################################################################################################################
echo "Open termux app on android"
adb shell am start -a android.intent.action.MAIN -c android.intent.category.LAUNCHER -n com.termux/.app.TermuxActivity
echo "termux app started"
echo "bashrc is configured to start sshd, refer to file \"/data/data/com.termux/files/usr/etc/bash.bashrc\" "

