#!/bin/bash

# Make sure adb run on ethernet, run only once
# adb connect ip_addr after reboot
adb shell setprop persist.adb.tcp.port 5555
