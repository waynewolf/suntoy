#!/bin/bash

############################################################################
# !!! Attention: copy and run me in the aosp root dir                      #
############################################################################

AOSP_ROOT=$(pwd)
echo "AOSP_ROOT $AOSP_ROOT"

cd hikey-linaro
# The kernel must be previously configured by:
# make ARCH=arm64 hikey960_defconfig
# make ARCH=arm64 menuconfig
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-android- -j4

cp arch/arm64/boot/dts/hisilicon/hi3660-hikey960.dtb $AOSP_ROOT/device/linaro/hikey-kernel
cp arch/arm64/boot/Image.gz $AOSP_ROOT/device/linaro/hikey-kernel/Image.gz-hikey960

echo "make bootimage"
cd $AOSP_ROOT
make bootimage -j4

echo "make dt image"
device/linaro/hikey/installer/hikey960/mkdtimg -c -d device/linaro/hikey-kernel/hi3660-hikey960.dtb -o out/target/product/hikey960/dt.img

echo -n "flash boot.img and dt.img (y/n)? "
old_stty_cfg=$(stty -g)
stty raw -echo; answer=$(head -c 1); stty $old_stty_cfg # Care playing with stty
if echo "$answer" | grep -iq "^y" ;then
	sudo fastboot flash boot out/target/product/hikey960/boot.img
	sudo fastboot flash dts out/target/product/hikey960/dt.img
fi


