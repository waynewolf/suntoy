#ifndef COMMAND_PARSER_H
#define COMMAND_PARSER_H

#include <Arduino.h>

/**********************************************************************
 * command format:
 * 
 *    type;name[;arg1[&arg2][&arg3]...]
 * 
 *    keyword args can be decomposed like this:
 *      x=1&y=2
 * 
 *    no leading and trailing blanks are allowed in the command line,
 *    if args are string, blanks in the middle are allowed.
 * 
 * examples:
 * 
 * 1. command from ctrlh -> ctrld
 *    c;setPwmDutyCycle;5000000
 * 2. syncrhonous  data from ctrld -> ctrlh
 *    d;err_code[;data]
 * 3. asynchronous data from ctrld -> ctrlh
 *    e;movement;x=1&y=2
 *
 **********************************************************************/

class CommandParser {
public:
  CommandParser();
  int parse(String command, char& type, char* name, char* args);

private:
  bool    _parsed;
  String  _storedCommand;
};

#endif
