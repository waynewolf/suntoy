#include "dc_motor.h"

MyDCMotor::MyDCMotor() :
  _adafruitMotorShield(Adafruit_MotorShield()) {
  // use port M1 and M2 of adafruit motor shield v2
  _motor1 = _adafruitMotorShield.getMotor(1);
  _motor2 = _adafruitMotorShield.getMotor(2);
}

void MyDCMotor::init(int freq) {
  _adafruitMotorShield.begin(freq);
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  _motor1->setSpeed(127);
  _motor1->run(FORWARD);
  // turn on motor 1
  _motor1->run(RELEASE);

  _motor2->setSpeed(127);
  _motor2->run(FORWARD);
  // turn on motor 2
  _motor2->run(RELEASE);
}

// type;name[;arg1[&arg2][&arg3]...]
// arg format: 1&150[&2&100]
// arg format: motorNo1&speed1[&motorNo2&speed2]
// speed within [0, 255]
int MyDCMotor::setMotorSpeed(char* args, char *ret) {
  int motorNo1 = -1, motorNo2 = -1;
  int speed1 = -1, speed2 = -1;
  
  if (!parseArgs(args, &motorNo1, &speed1, &motorNo2, &speed2)) {
    Serial.println("setMotorSpeed argument parse error");
    strncpy(ret, "setMotorSpeed argument parse error", 63);
    return -1;
  }

  if (motorNo1 != -1 && speed1 != -1) {
    if (motorNo1 == 1)
      _motor1->setSpeed(speed1);
    else if (motorNo1 == 2)
      _motor2->setSpeed(speed1);
  }

  if (motorNo2 != -1 && speed2 != -1) {
    if (motorNo2 == 1)
      _motor1->setSpeed(speed2);
    else if (motorNo2 == 2)
      _motor2->setSpeed(speed2);
  }

  delay(10);
  ret[0] = '\0';
  return 0;
}

// type;name[;arg1[&arg2][&arg3]...]
// arg format: motorNo1&direction1[&motorNo2&direction2]
// 0 - forward; 1 - backword; 2 - release
int MyDCMotor::setMotorMotion(char* args, char *ret) {
  int motorNo1 = -1, motorNo2 = -1;
  int direction1 = -1, direction2 = -1;
  
  if (!parseArgs(args, &motorNo1, &direction1, &motorNo2, &direction2)) {
    Serial.println("setMotorMotion argument parse error");
    strncpy(ret, "setMotorMotion argument parse error", 63);
    return -1;
  }

  if (motorNo1 != -1 && direction1 != -1) {
    if (motorNo1 == 1) 
      _motor1->run(convertDirection(direction1));
    else if (motorNo1 == 2)
      _motor2->run(convertDirection(direction1));
  }

  if (motorNo2 != -1 && direction2 != -1) {
    if (motorNo2 == 1)
      _motor1->run(convertDirection(direction2));
    else if (motorNo2 == 2)
      _motor2->run(convertDirection(direction2));
  }

  delay(10);
  ret[0] = '\0';
  return 0;
}

int MyDCMotor::convertDirection(int direction) {
  if (direction == 0) return BACKWARD;
  if (direction == 1) return FORWARD;
  return RELEASE;
}

bool MyDCMotor::parseArgs(char* args, int *motorNo1, int *param1, int *motorNo2, int *param2) {
  if (!args) return false;

  int temp = -1;
  char *pch = strtok (args, "&");
  if (!pch) return false;
  temp = atoi(pch);

  if (temp != 1 && temp != 2) return false;
  *motorNo1 = temp;

  pch = strtok (NULL, "&");
  if (!pch) return false;
  *param1 = atoi(pch);
  
  pch = strtok (NULL, "&");
  // no optional argument for second motor, it's okay, don't touch other input parameters
  if (!pch) return true;
  // we have arguments for second motor
  temp = atoi(pch);
  if ((temp != 1 && temp != 2) || temp == *motorNo1) return false;
  *motorNo2 = temp;
  
  pch = strtok (NULL, "&");
  if (!pch) return false;
  *param2 = atoi(pch);

  return true;
}

