#include "myservo.h"

MyServo::MyServo() {
  _attached = false;
}

void MyServo::openServo(int pin) {
  attach(pin);
  _attached = true;
}

int MyServo::openServo(char* args, char *ret) {
  if (!args) {
    Serial.println("openServo argument NULL");
    strncpy(ret, "openServo argument NULL", 63);
    return -1;
  }

  int pin = atoi(args);
  openServo(pin);

  ret[0] = '\0';
  return 0;
}

int MyServo::closeServo(char* args, char *ret) {
  args = 0;
  detach();
  _attached = false;

  ret[0] = '\0';
  return 0;
}

int MyServo::setServoAngle(int angle) {
  if (angle < 0 || angle > 180) {
    Serial.println("setServoAngle angle should be within [0, 180]");
    return -1;
  }

  Servo::write(angle);
  return 0;
}

int MyServo::setServoAngle(char* args, char *ret) {
  if (!args) {
    Serial.println("setServoAngle argument NULL");
    strncpy(ret, "setServoAngle argument NULL", 63);
    return -1;
  }
  if (!_attached) {
    Serial.println("setServoAngle servo not attached");
    strncpy(ret, "setServoAngle servo not attached", 63);
    return -2;
  }
  int angle = atoi(args);
  int retVal = setServoAngle(angle);
  delay(15);

  if (retVal < 0)
    strncpy(ret, "setServoAngle failed", 63);
  else
    ret[0] = '\0';
  return retVal;
}

int MyServo::getServoAngle() {
  return Servo::read();
}

int MyServo::getServoAngle(char* args, char *ret) {
  args = 0;

  if (!_attached) {
    Serial.println("getServoAngle servo not attached");
    strncpy(ret, "getServoAngle servo not attached", 63);
    return -2;
  }
  
  int angle = getServoAngle();
  snprintf(ret, 63, "%d", angle);
  return 0;
}

