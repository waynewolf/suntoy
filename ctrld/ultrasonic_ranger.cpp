#include "ultrasonic_ranger.h"

UltraSonicRanger::UltraSonicRanger() {
  
}

void UltraSonicRanger::attachPin(int trig, int echo, int led) {
  pinMode(echo, INPUT);
  pinMode(trig, OUTPUT);
  pinMode(led,  OUTPUT);

  _trigPin = trig;
  _echoPin = echo;
  _ledPin  = led;
}

void UltraSonicRanger::trig() {
  // send 10ms pulse to trig pin
  digitalWrite(_trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(_trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(_trigPin, LOW);
}

float UltraSonicRanger::getDistance() {
  trig();
  float distance = pulseIn(_echoPin, HIGH);
  distance = distance/58.0;
  if(distance < 10) {
    digitalWrite(_ledPin, HIGH);
  } else {
    digitalWrite(_ledPin, LOW);
  }

  // return distance in cm
  return distance;
}

int UltraSonicRanger::getDistance(char* args, char* ret) {
  args = 0;
  int distance = (int)getDistance();

  snprintf(ret, 63, "d;dist;%d", distance);
  return 0;
}

