#ifndef PWM_H
#define PWM_H

class Pwm {
public:
  Pwm();
  int setPwmDutyCycle(char *args, char *ret);
  int getPwmDutyCycle(char *args, char *ret);
  int setPwmPeriod(char *args, char *ret);
  int getPwmPeriod(char *args, char *ret);

private:
  int _dutyCycle;
  int _period;
};

#endif
