#ifndef MYSERVO_H
#define MYSERVO_H

#include <Arduino.h>
#include "servo2.h"

class MyServo : public Servo {
public:
  MyServo();
  void openServo(int pin);
  int openServo(char* args, char *ret);
  int closeServo(char* args, char *ret);
  int setServoAngle(int angle);
  int setServoAngle(char* args, char *ret);
  int getServoAngle();
  int getServoAngle(char* args, char *ret);

private:
  bool _attached;
};

#endif
