#include <SoftwareSerial.h>
#include "command_parser.h"
#include "pwm.h"
#include "myservo.h"
#include "ultrasonic_ranger.h"
#include "dc_motor.h"

/**********************************************************************
 * pin map
 **********************************************************************/
const int PIN_BT_RX           = 5;
const int PIN_BT_TX           = 6;
const int PIN_HC_SR04_ECHO    = 13;
const int PIN_HC_SR04_TRIG    = 2;
const int PIN_DISTANCE_LED    = 14;
const int PIN_SERVO           = 9;

/**********************************************************************
 * constants
 **********************************************************************/
const int BT_BAUD_RATE        = 9600;

/**********************************************************************
 * global variabls
 **********************************************************************/
String            cmdFromHost;
CommandParser     cmdParser;
Pwm               pwm;
MyServo           servo;
UltraSonicRanger  hcSr04;
char              message[128]    = {0};
char              retMessage[64]  = {0};
SoftwareSerial    bluetooth(PIN_BT_RX, PIN_BT_TX);
MyDCMotor         motor;

#define COMMAND_DEFINE(OBJ, NAME)                   \
int wrapper_##NAME(char *args, char* ret) {   \
  return OBJ.NAME(args, ret);                       \
}

#define COMMAND_ITEM(TYPE, NAME)    \
  {TYPE, #NAME, wrapper_##NAME}

COMMAND_DEFINE(pwm, setPwmDutyCycle)
COMMAND_DEFINE(pwm, getPwmDutyCycle)
COMMAND_DEFINE(pwm, setPwmPeriod)
COMMAND_DEFINE(pwm, getPwmPeriod)
COMMAND_DEFINE(servo, openServo)
COMMAND_DEFINE(servo, closeServo)
COMMAND_DEFINE(servo, setServoAngle)
COMMAND_DEFINE(servo, getServoAngle)
COMMAND_DEFINE(hcSr04, getDistance)
COMMAND_DEFINE(motor, setMotorSpeed)
COMMAND_DEFINE(motor, setMotorMotion)

typedef int (*PFN_COMMAND_HANDLER)(char *args, char *ret);

static struct sHandlerMap{
  char type;
  const char *name;
  PFN_COMMAND_HANDLER handler;
} handlerMap[] = {
  COMMAND_ITEM('c', setPwmDutyCycle),
  COMMAND_ITEM('c', getPwmDutyCycle),
  COMMAND_ITEM('c', setPwmPeriod),
  COMMAND_ITEM('c', getPwmPeriod),
  COMMAND_ITEM('c', openServo),
  COMMAND_ITEM('c', closeServo),
  COMMAND_ITEM('c', setServoAngle),
  COMMAND_ITEM('c', getServoAngle),
  COMMAND_ITEM('c', getDistance),
  COMMAND_ITEM('c', setMotorSpeed),
  COMMAND_ITEM('c', setMotorMotion)
};

/**********************************************************************
 * functions
 **********************************************************************/
int handleCommand(char type, const char *name, char *args, char *ret) {
  int handlerNumber = sizeof(handlerMap)/sizeof(handlerMap[0]);
  for (int i = 0; i < handlerNumber; i++) {
    if (type == handlerMap[i].type && !strcmp(name, handlerMap[i].name)) {
      return handlerMap[i].handler(args, ret);
    }
  }

  snprintf(ret, 63, "command name %s not supported", name);
  Serial.println(ret);
  snprintf(ret, 63, "command name %s not supported", name);
  return -1;
}

void setup() {
  Serial.begin(9600);
  Serial.setTimeout(100);
  motor.init();
  bluetooth.begin(BT_BAUD_RATE);
  // 9600 bps means 120 bytes(characters) can be sent within 100ms, 100ms is a reasonable timeout value
  bluetooth.setTimeout(100);
  servo.openServo(PIN_SERVO);
  hcSr04.attachPin(PIN_HC_SR04_TRIG, PIN_HC_SR04_ECHO, PIN_DISTANCE_LED);
}

void loop() {
  // handle synchronous command from ctrlh
  bool serialHasData = Serial.available() > 0;
  bool btHasData = bluetooth.available() > 0;
  if (serialHasData || btHasData) {
    if (btHasData)
      cmdFromHost = bluetooth.readStringUntil('\n');
    else
      cmdFromHost = Serial.readStringUntil('\n');

    char type, name[16], args[32];
    cmdParser.parse(cmdFromHost, type, name, args);
    int ret = handleCommand(type, name, args, retMessage);
    bool hasRetMessage = retMessage[0] != 0;
    if (hasRetMessage)
      snprintf(message, 63, "d;%d;%s", ret, retMessage);
    else
      snprintf(message, 63, "d;%d", ret);

    // synchronous response|data echo back to ctrlh
    if (btHasData)
      bluetooth.println(message);
    else
      Serial.println(message);
  }

  delay(60);
}

