#ifndef DC_MOTOR_H
#define DC_MOTOR_H

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

/*
 * Use adafruit motor shield v2, at the default I2C address
 */
class MyDCMotor {
public:
  MyDCMotor();
  // default frequency 1.6KHz
  void init(int freq=1600);
  int setMotorSpeed(char* args, char *ret);
  int setMotorMotion(char* args, char *ret);

private:
  bool parseArgs(char* args, int *motorNo1, int *param1, int *motorNo2, int *param2);
  int convertDirection(int direction);
  
private:
  Adafruit_MotorShield _adafruitMotorShield;
  Adafruit_DCMotor *_motor1;
  Adafruit_DCMotor *_motor2;
};

#endif
