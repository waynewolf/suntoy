#include "command_parser.h"

CommandParser::CommandParser() :
  _parsed(false),
  _storedCommand("") {
  
}

int CommandParser::parse(String command, char& type, char* name, char* args) {
  char cmd[64] = {0};
  strcpy(cmd, command.c_str());
  
  char *p = cmd;
  char *tok = strsep(&p, ";");
  if (tok == NULL || strlen(tok) != 1) {
    Serial.println("command type parse error");
    return -1;
  }
  type = tok[0];

  tok = strsep(&p, ";");
  if (tok == NULL || strlen(tok) == 0) {
    Serial.println("command name parse error");
    return -1;
  }
  strcpy(name, tok);

  tok = strsep(&p, ";");
  if (tok != NULL)
    strcpy(args, tok);

  _parsed = true;

  return 0;
}

