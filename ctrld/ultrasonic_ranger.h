#ifndef ULTRASONIC_RANGER_H
#define ULTRASONIC_RANGER_H

#include <Arduino.h>

class UltraSonicRanger {
public:
  UltraSonicRanger();
  void attachPin(int trig, int echo, int led = LED_BUILTIN);
  void trig();
  float getDistance();
  int getDistance(char* args, char* ret);

private:
  int _ledPin;
  int _trigPin;
  int _echoPin;
};

#endif
