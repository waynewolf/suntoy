package com.weirong.suntoy.ctrlh.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.weirong.suntoy.ctrlh.R;

public class TerminalModeFragment extends Fragment {
  private static final String TAG = "TerminalModeFragment";
  private EditText _editText = null;
  private Button _sendBtn = null;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_terminal_mode, container, false);
    _editText = (EditText)v.findViewById(R.id.edit_text);
    _sendBtn = (Button)v.findViewById(R.id.send_btn);

    _sendBtn.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        View view = getView();
        if (null != view) {
          String command = _editText.getText().toString();
          sendCommand(command);
        }
      }
    });

    return v;
  }
  
  public void sendCommand(String command) {
    Log.i(TAG, command);
    ((MainActivity)getActivity()).sendCommand(command);
  }
}
