package com.weirong.suntoy.ctrlh.activity;

import android.app.Fragment;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.erz.joysticklibrary.JoyStick;
import com.weirong.suntoy.ctrlh.CmdTextBuilder;
import com.weirong.suntoy.ctrlh.R;

import java.util.Date;

public class ControllerFragment extends Fragment implements JoyStick.JoyStickListener {
  private static final String TAG="ControllerFragment";
  private static final int COMMAND_INTERNAL_MS = 50;
  private long _lastTime;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_controller, container, false);

    JoyStick joy1 = (JoyStick) v.findViewById(R.id.joy1);
    joy1.setListener(this);
    joy1.setPadColor(Color.parseColor("#55ffffff"));
    joy1.setButtonColor(Color.parseColor("#55ff0000"));

    JoyStick joy2 = (JoyStick) v.findViewById(R.id.joy2);
    joy2.setListener(this);
    joy2.enableStayPut(true);
    joy2.setPadBackground(R.drawable.pad);
    joy2.setButtonDrawable(R.drawable.button);

    return v;
  }

  @Override
  public void onMove(JoyStick joyStick, double angle, double power, int direction) {
    switch (joyStick.getId()) {
      case R.id.joy1:
        Log.i(TAG, "move angle: " + angle + ", power: " + power + ", direction: " + direction);
        movement(angle, power, direction);
        break;
      case R.id.joy2:
        Log.i(TAG, "rotate angle: " + angle);
        break;
    }
  }

  @Override
  public void onTap() {
    Log.i(TAG, "tap");
  }

  @Override
  public void onDoubleTap() {
    Log.i(TAG, "double tap");
  }

  private void movement(double angle, double power, int direction) {
    Date date = Calendar.getInstance().getTime();
    long now = date.getTime();
    if (now - _lastTime < COMMAND_INTERNAL_MS) {
      _lastTime = now;
      return;
    }
    _lastTime = now;

    // algo: use power to calculate speed, use direction to control rotate
    int speed = lerp(power, 0.0, 100.0, 0, 255);
    double motor1SpeedFactor = 0;
    double motor2SpeedFactor = 0;
    int moveDirection = 2; //stop

    /*      2
     *   1     3
     * 0 <- o -> 4
     *   7     5
     *      6
     */
    switch(direction) {
      case 0:
        motor1SpeedFactor = 0;
        motor2SpeedFactor = 1;
        moveDirection = 1; // forward
        break;
      case 1:
        motor1SpeedFactor = 0.5;
        motor2SpeedFactor = 1;
        moveDirection = 1; // forward
        break;
      case 2:
        motor1SpeedFactor = 1;
        motor2SpeedFactor = 1;
        moveDirection = 1; // forward
        break;
      case 3:
        motor1SpeedFactor = 1;
        motor2SpeedFactor = 0.5;
        moveDirection = 1; // forward
        break;
      case 4:
        motor1SpeedFactor = 1;
        motor2SpeedFactor = 0;
        moveDirection = 1; // forward
        break;
      case 5:
        motor1SpeedFactor = 1;
        motor2SpeedFactor = 0.5;
        moveDirection = 0; // backward
        break;
      case 6:
        motor1SpeedFactor = 1;
        motor2SpeedFactor = 1;
        moveDirection = 0; // backward
        break;
      case 7:
        motor1SpeedFactor = 0.5;
        motor2SpeedFactor = 1;
        moveDirection = 0; // backward
        break;
      default:
        return;
    }

    // give it a bit more chance to stop
    if (power < 1)
      moveDirection = 2;

    String command = CmdTextBuilder.build('c', "setMotorSpeed", 1, (int)(speed * motor1SpeedFactor), 2, (int)(speed * motor2SpeedFactor));
    sendCommand(command);

    command = CmdTextBuilder.build('c', "setMotorMotion", 1, moveDirection, 2, moveDirection);
    sendCommand(command);
  }

  /*
   *    (intput - minInput)/(maxInput - minInput) == (output - minOutput)/(maxOuput - minOutput)
   * => output = (input - minInput) / (maxInput - minInput) * (maxOutput - minOutput) + minOutput
   */
  private int lerp(double input, double minInput, double maxInput, int minOutput, int maxOutput) {
    double output = output = (input - minInput) / (maxInput - minInput) * (maxOutput - minOutput) + minOutput;
    return (int)output;
  }

  private int lerp(double input, double minInput, double maxInput, double minOutput, double maxOutput) {
    double output = output = (input - minInput) / (maxInput - minInput) * (maxOutput - minOutput) + minOutput;
    return (int)output;
  }

  private void sendCommand(String command) {
    ((MainActivity)getActivity()).sendCommand(command);
  }
}
