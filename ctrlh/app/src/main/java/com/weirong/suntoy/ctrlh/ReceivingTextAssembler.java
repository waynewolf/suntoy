package com.weirong.suntoy.ctrlh;

import android.util.Log;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class ReceivingTextAssembler {
  private static final String TAG = "ReceivingAssembler";
  private StringBuilder _incompleteText = new StringBuilder();
  private TextProtoCallback _textProtoCallback = null;

  public ReceivingTextAssembler(TextProtoCallback cb) {
    _textProtoCallback = cb;
  }

  public void addText(String textFromDevice) {
    int originalTextLength = textFromDevice.length();
    int firstIndexOfNewLine = textFromDevice.indexOf('\n');
    if (firstIndexOfNewLine == -1) {
      // just append this text, as this is just partial text from device
      _incompleteText.append(textFromDevice);
    }
    else {
      // append the text before '\n'
      String subStringBeforeFirstNewLine = textFromDevice.substring(0, firstIndexOfNewLine);
      _incompleteText.append(subStringBeforeFirstNewLine);
      String shouldBeCompleteText = _incompleteText.toString();
      if (shouldBeCompleteText.endsWith("\r")) {
        shouldBeCompleteText = shouldBeCompleteText.substring(0, shouldBeCompleteText.length() - 1);
        originalTextLength -= 1;
      }
      if (isValidReply(shouldBeCompleteText)) {
        _textProtoCallback.onReceiveReply(shouldBeCompleteText);
      }
      // clear the string builder
      _incompleteText.setLength(0);
      // process substring after the first '\n', recurisve as it may contain other '\n'.
      if (firstIndexOfNewLine < originalTextLength - 1) {
        String subStringAfterFirstNewLine = textFromDevice.substring(firstIndexOfNewLine + 1, originalTextLength);
        addText(subStringAfterFirstNewLine);
      }
    }
  }

  public static boolean isValidReply(String message) {
    char type = '\0';
    String error_code = "";
    String optArg = "";
    StringTokenizer st = new StringTokenizer(message, ";");
    if (st.hasMoreTokens()) type = st.nextToken().charAt(0);
    if (st.hasMoreTokens()) error_code = st.nextToken();
    if (st.hasMoreTokens()) optArg = st.nextToken();

    if (type != 'd')
      return false;
    try {
      Integer.parseInt(error_code);
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  public static int getReplyErrorCode(String validReply) {
    StringTokenizer st = new StringTokenizer(validReply, ";");
    st.nextToken();
    return Integer.parseInt(st.nextToken());
  }

  public static String getReplyOptionalData(String validReply) {
    StringTokenizer st = new StringTokenizer(validReply, ";");
    st.nextToken();
    return st.nextToken();
  }

}
