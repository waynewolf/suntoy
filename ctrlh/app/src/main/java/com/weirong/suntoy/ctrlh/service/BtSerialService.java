package com.weirong.suntoy.ctrlh.service;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class BtSerialService {
  private static final String TAG = "BtSerialService";

  // Name for the SDP record when creating server socket
  private static final String NAME_SECURE = "BtSerialServiceSecure";
  private static final String NAME_INSECURE = "BtSerialServiceInsecure";

  //    private static final UUID MY_UUID_SECURE =
  //            UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
  //    private static final UUID MY_UUID_INSECURE =
  //            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
  // using the well-known SPP UUID 00001101-0000-1000-8000-00805F9B34FB for bluetooth serial port.
  private static final UUID MY_UUID_SECURE =
      UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
  private static final UUID MY_UUID_INSECURE =
          UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

  private final BluetoothAdapter _btAdapter;
  private final Handler _serviceHandler;
  private AcceptThread _secureAcceptThread;
  private AcceptThread _insecureAcceptThread;
  private ConnectThread _connectThread;
  private ConnectedThread _connectedThread;
  private int _state;
  private int _newState;

  public static final int STATE_NONE = 0;       // we're doing nothing
  public static final int STATE_LISTEN = 1;     // now listening for incoming connections
  public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
  public static final int STATE_CONNECTED = 3;  // now connected to a remote device

  public BtSerialService(Context context, Handler handler) {
    _btAdapter = BluetoothAdapter.getDefaultAdapter();
    _state = STATE_NONE;
    _newState = _state;
    _serviceHandler = handler;
  }

  /**
   * Update UI title according to the current state of the chat connection
   */
  private synchronized void updateUserInterfaceTitle() {
    _state = getState();
    Log.d(TAG, "updateUserInterfaceTitle() " + _newState + " -> " + _state);
    _newState = _state;

    // Give the new state to the Handler so the UI Activity can update
    _serviceHandler.obtainMessage(Constants.MESSAGE_STATE_CHANGE, _newState, -1).sendToTarget();
  }

  /**
   * Return the current connection state.
   */
  public synchronized int getState() {
      return _state;
  }

  /**
  * Start the chat service. Specifically start AcceptThread to begin a
  * session in listening (server) mode. Called by the Activity onResume()
  */
  public synchronized void start() {
    Log.d(TAG, "start");

    // Cancel any thread attempting to make a connection
    if (_connectThread != null) {
      _connectThread.cancel();
      _connectThread = null;
    }

    // Cancel any thread currently running a connection
    if (_connectedThread != null) {
      _connectedThread.cancel();
      _connectedThread = null;
    }

    // Start the thread to listen on a BluetoothServerSocket
    if (_secureAcceptThread == null) {
      _secureAcceptThread = new AcceptThread(true);
      _secureAcceptThread.start();
    }
    if (_insecureAcceptThread == null) {
      _insecureAcceptThread = new AcceptThread(false);
      _insecureAcceptThread.start();
    }
    // Update UI title
    updateUserInterfaceTitle();
  }

  /**
   * Start the ConnectThread to initiate a connection to a remote device.
   *
   * @param device The BluetoothDevice to connect
   * @param secure Socket Security type - Secure (true) , Insecure (false)
   */
  public synchronized void connect(BluetoothDevice device, boolean secure) {
    Log.d(TAG, "connect to: " + device);

    // Cancel any thread attempting to make a connection
    if (_state == STATE_CONNECTING) {
      if (_connectThread != null) {
          _connectThread.cancel();
          _connectThread = null;
      }
    }

    // Cancel any thread currently running a connection
    if (_connectedThread != null) {
      _connectedThread.cancel();
      _connectedThread = null;
    }

    // Start the thread to connect with the given device
    _connectThread = new ConnectThread(device, secure);
    _connectThread.start();
    // Update UI title
    updateUserInterfaceTitle();
  }

  /**
   * Start the ConnectedThread to begin managing a Bluetooth connection
   *
   * @param socket The BluetoothSocket on which the connection was made
   * @param device The BluetoothDevice that has been connected
   */
  public synchronized void connected(BluetoothSocket socket, BluetoothDevice
          device, final String socketType) {
    Log.d(TAG, "connected, Socket Type:" + socketType);

    // Cancel the thread that completed the connection
    if (_connectThread != null) {
      _connectThread.cancel();
      _connectThread = null;
    }

    // Cancel any thread currently running a connection
    if (_connectedThread != null) {
      _connectedThread.cancel();
      _connectedThread = null;
    }

    // Cancel the accept thread because we only want to connect to one device
    if (_secureAcceptThread != null) {
      _secureAcceptThread.cancel();
      _secureAcceptThread = null;
    }
    if (_insecureAcceptThread != null) {
      _insecureAcceptThread.cancel();
      _insecureAcceptThread = null;
    }

    // Start the thread to manage the connection and perform transmissions
    _connectedThread = new ConnectedThread(socket, socketType);
    _connectedThread.start();

    // Send the name of the connected device back to the UI Activity
    Message msg = _serviceHandler.obtainMessage(Constants.MESSAGE_DEVICE_NAME);
    Bundle bundle = new Bundle();
    bundle.putString(Constants.DEVICE_NAME, device.getName());
    msg.setData(bundle);
    _serviceHandler.sendMessage(msg);
    // Update UI title
    updateUserInterfaceTitle();
  }

  /**
   * Stop all threads
   */
  public synchronized void stop() {
    Log.d(TAG, "stop");

    if (_connectThread != null) {
      _connectThread.cancel();
      _connectThread = null;
    }

    if (_connectedThread != null) {
      _connectedThread.cancel();
      _connectedThread = null;
    }

    if (_secureAcceptThread != null) {
      _secureAcceptThread.cancel();
      _secureAcceptThread = null;
    }

    if (_insecureAcceptThread != null) {
      _insecureAcceptThread.cancel();
      _insecureAcceptThread = null;
    }
    _state = STATE_NONE;
    // Update UI title
    updateUserInterfaceTitle();
  }

  /**
   * Write to the ConnectedThread in an unsynchronized manner
   *
   * @param out The bytes to write
   * @see ConnectedThread#write(byte[])
   */
  public void write(byte[] out) {
    // Create temporary object
    ConnectedThread r;
    // Synchronize a copy of the ConnectedThread
    synchronized (this) {
      if (_state != STATE_CONNECTED) return;
      r = _connectedThread;
    }
    // Perform the write unsynchronized
    r.write(out);
  }

  /**
   * Indicate that the connection attempt failed and notify the UI Activity.
   */
  private void connectionFailed() {
    // Send a failure message back to the Activity
    Message msg = _serviceHandler.obtainMessage(Constants.MESSAGE_TOAST);
    Bundle bundle = new Bundle();
    bundle.putString(Constants.TOAST, "Unable to connect device");
    msg.setData(bundle);
    _serviceHandler.sendMessage(msg);

    _state = STATE_NONE;
    // Update UI title
    updateUserInterfaceTitle();

    // Start the service over to restart listening mode
    BtSerialService.this.start();
  }

  /**
   * Indicate that the connection was lost and notify the UI Activity.
   */
  private void connectionLost() {
    // Send a failure message back to the Activity
    Message msg = _serviceHandler.obtainMessage(Constants.MESSAGE_TOAST);
    Bundle bundle = new Bundle();
    bundle.putString(Constants.TOAST, "Device connection was lost");
    msg.setData(bundle);
    _serviceHandler.sendMessage(msg);

    _state = STATE_NONE;
    // Update UI title
    updateUserInterfaceTitle();

    // Start the service over to restart listening mode
    BtSerialService.this.start();
  }

  /**
   * This thread runs while listening for incoming connections. It behaves
   * like a server-side client. It runs until a connection is accepted
   * (or until cancelled).
   */
  private class AcceptThread extends Thread {
    // The local server socket
    private final BluetoothServerSocket mmServerSocket;
    private String mSocketType;

    public AcceptThread(boolean secure) {
      BluetoothServerSocket tmp = null;
      mSocketType = secure ? "Secure" : "Insecure";

      // Create a new listening server socket
      try {
        if (secure) {
          tmp = _btAdapter.listenUsingRfcommWithServiceRecord(NAME_SECURE,
                  MY_UUID_SECURE);
        } else {
          tmp = _btAdapter.listenUsingInsecureRfcommWithServiceRecord(
                  NAME_INSECURE, MY_UUID_INSECURE);
        }
      } catch (IOException e) {
        Log.e(TAG, "Socket Type: " + mSocketType + "listen() failed", e);
      }
      mmServerSocket = tmp;
      _state = STATE_LISTEN;
    }

    public void run() {
      Log.d(TAG, "Socket Type: " + mSocketType + "BEGIN mAcceptThread" + this);
      setName("AcceptThread" + mSocketType);

      BluetoothSocket socket = null;

      // Listen to the server socket if we're not connected
      while (_state != STATE_CONNECTED) {
        try {
          // This is a blocking call and will only return on a
          // successful connection or an exception
          socket = mmServerSocket.accept();
        } catch (IOException e) {
          Log.e(TAG, "Socket Type: " + mSocketType + "accept() failed", e);
          break;
        }

        // If a connection was accepted
        if (socket != null) {
          synchronized (BtSerialService.this) {
            switch (_state) {
              case STATE_LISTEN:
              case STATE_CONNECTING:
                // Situation normal. Start the connected thread.
                connected(socket, socket.getRemoteDevice(),
                        mSocketType);
                break;
              case STATE_NONE:
              case STATE_CONNECTED:
                // Either not ready or already connected. Terminate new socket.
                try {
                    socket.close();
                } catch (IOException e) {
                    Log.e(TAG, "Could not close unwanted socket", e);
                }
                break;
            }
          }
        }
      }
      Log.i(TAG, "END mAcceptThread, socket Type: " + mSocketType);
    }

    public void cancel() {
      Log.d(TAG, "Socket Type" + mSocketType + "cancel " + this);
      try {
        mmServerSocket.close();
      } catch (IOException e) {
        Log.e(TAG, "Socket Type" + mSocketType + "close() of server failed", e);
      }
    }
  }

  /**
   * This thread runs while attempting to make an outgoing connection
   * with a device. It runs straight through; the connection either
   * succeeds or fails.
   */
  private class ConnectThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private String mSocketType;

    public ConnectThread(BluetoothDevice device, boolean secure) {
      mmDevice = device;
      BluetoothSocket tmp = null;
      mSocketType = secure ? "Secure" : "Insecure";

      // Get a BluetoothSocket for a connection with the
      // given BluetoothDevice
      try {
        if (secure) {
            tmp = device.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
        } else {
            tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE);
        }
      } catch (IOException e) {
        Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
      }
      mmSocket = tmp;
      _state = STATE_CONNECTING;
    }

    public void run() {
      Log.i(TAG, "BEGIN _connectThread SocketType:" + mSocketType);
      setName("ConnectThread" + mSocketType);

      // Always cancel discovery because it will slow down a connection
      _btAdapter.cancelDiscovery();

      // Make a connection to the BluetoothSocket
      try {
        // This is a blocking call and will only return on a
        // successful connection or an exception
        mmSocket.connect();
      } catch (IOException e) {
        // Close the socket
        try {
          mmSocket.close();
        } catch (IOException e2) {
          Log.e(TAG, "unable to close() " + mSocketType + " socket during connection failure", e2);
        }
        connectionFailed();
        return;
      }

      // Reset the ConnectThread because we're done
      synchronized (BtSerialService.this) {
        _connectThread = null;
      }

      // Start the connected thread
      connected(mmSocket, mmDevice, mSocketType);
    }

    public void cancel() {
      try {
        mmSocket.close();
      } catch (IOException e) {
        Log.e(TAG, "close() of connect " + mSocketType + " socket failed", e);
      }
    }

  }

  /**
   * This thread runs during a connection with a remote device.
   * It handles all incoming and outgoing transmissions.
   */
  private class ConnectedThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;

    public ConnectedThread(BluetoothSocket socket, String socketType) {
      Log.d(TAG, "create ConnectedThread: " + socketType);
      mmSocket = socket;
      InputStream tmpIn = null;
      OutputStream tmpOut = null;

      // Get the BluetoothSocket input and output streams
      try {
        tmpIn = socket.getInputStream();
        tmpOut = socket.getOutputStream();
      } catch (IOException e) {
        Log.e(TAG, "temp sockets not created", e);
      }

      mmInStream = tmpIn;
      mmOutStream = tmpOut;
      _state = STATE_CONNECTED;
    }

    public void run() {
      Log.i(TAG, "BEGIN _connectedThread");
      byte[] buffer = new byte[1024];
      int bytes;

      // Keep listening to the InputStream while connected
      while (_state == STATE_CONNECTED) {
        try {
          // Read from the InputStream
          bytes = mmInStream.read(buffer);

          // Send the obtained bytes to the UI Activity
          _serviceHandler.obtainMessage(Constants.MESSAGE_READ, bytes, -1, buffer).sendToTarget();
        } catch (IOException e) {
          Log.e(TAG, "disconnected", e);
          connectionLost();
          break;
        }
      }
    }

    /**
     * Write to the connected OutStream.
     *
     * @param buffer The bytes to write
     */
    public void write(byte[] buffer) {
      try {
        mmOutStream.write(buffer);

        // Share the sent message back to the UI Activity
        _serviceHandler.obtainMessage(Constants.MESSAGE_WRITE, -1, -1, buffer).sendToTarget();
      } catch (IOException e) {
        Log.e(TAG, "Exception during write", e);
      }
    }

    public void cancel() {
      try {
        mmSocket.close();
      } catch (IOException e) {
        Log.e(TAG, "close() of connect socket failed", e);
      }
    }
  }
}
