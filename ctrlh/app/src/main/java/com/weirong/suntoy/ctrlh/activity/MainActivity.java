package com.weirong.suntoy.ctrlh.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;
import android.widget.ViewAnimator;

import com.weirong.suntoy.ctrlh.R;

public class MainActivity extends BaseActivity {
  public static final String TAG = "MainActivity";
  private MessageFragment _messageFragment = null;
  private boolean _terminalMode = false;
  private Button _clearButton = null;
  private ToggleButton _pauseButton = null;
  private boolean _pausedMessage = false;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    _messageFragment = (MessageFragment)getFragmentManager().findFragmentById(R.id.log_fragment);
    _clearButton = (Button)findViewById(R.id.clear_btn);
    _pauseButton = (ToggleButton)findViewById(R.id.pause_btn);

    _clearButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        clearMessage();
      }
    });

    _pauseButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        if (_pauseButton.isChecked()) {
          pauseMessage();
        } else {
          restartMessage();
        }
      }
    });

    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    MenuItem logToggle = menu.findItem(R.id.menu_toggle_log);
    logToggle.setVisible(findViewById(R.id.switch_mode) instanceof ViewAnimator);
    logToggle.setTitle(_terminalMode ? R.string.goto_normal_mode : R.string.goto_terminal_mode);

    return super.onPrepareOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case R.id.menu_toggle_log:
        _terminalMode = !_terminalMode;
        ViewAnimator output = (ViewAnimator) findViewById(R.id.switch_mode);
        if (_terminalMode) {
          output.setDisplayedChild(1);
        } else {
          output.setDisplayedChild(0);
        }
        supportInvalidateOptionsMenu();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  public void pauseMessage() {
    _pausedMessage = true;
  }

  public void restartMessage() {
    _pausedMessage = false;
  }

  @Override
  protected void clearMessage() {
    _messageFragment.clearText();
  }

  @Override
  protected void addMessage(String text) {
    if (!_pausedMessage) _messageFragment.addText(text);
  }
}
