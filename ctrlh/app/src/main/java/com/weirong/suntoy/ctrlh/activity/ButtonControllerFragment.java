package com.weirong.suntoy.ctrlh.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;

import com.weirong.suntoy.ctrlh.CmdTextBuilder;
import com.weirong.suntoy.ctrlh.R;

public class ButtonControllerFragment extends Fragment {
  private static final String TAG = "ControllerFragment";
  private static final double SPEED_DIFF_FACTOR = 0.2;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_button_controller, container, false);

    ImageButton up    = (ImageButton)v.findViewById(R.id.imageButtonUp);
    ImageButton down  = (ImageButton)v.findViewById(R.id.imageButtonDown);
    ImageButton left  = (ImageButton)v.findViewById(R.id.imageButtonLeft);
    ImageButton right = (ImageButton)v.findViewById(R.id.imageButtonRight);
    final SeekBar seekBar   = (SeekBar)v.findViewById(R.id.speedSeekBar);

    seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {
        int progress = seekBar.getProgress();
        int speed = lerp(progress, 0, 100, 0, 255);
        String command = CmdTextBuilder.build('c', "setMotorSpeed", 1, speed, 2, speed);
        sendCommand(command);
      }
    });

    up.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        switch(event.getAction()) {
          case MotionEvent.ACTION_DOWN:
            moveForward();
            return true;
          case MotionEvent.ACTION_UP:
            moveStop();
            return true;
        }
        return false;
      }
    });

    down.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        switch(event.getAction()) {
          case MotionEvent.ACTION_DOWN:
            moveBackward();
            return true;
          case MotionEvent.ACTION_UP:
            moveStop();
            return true;
        }
        return false;
      }
    });

    left.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        switch(event.getAction()) {
          case MotionEvent.ACTION_DOWN:
            rotateLeft(seekBar.getProgress());
            return true;
          case MotionEvent.ACTION_UP:
            rotateStop(seekBar.getProgress());
            return true;
        }
        return false;
      }
    });

    right.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        switch(event.getAction()) {
          case MotionEvent.ACTION_DOWN:
            rotateRight(seekBar.getProgress());
            return true;
          case MotionEvent.ACTION_UP:
            rotateStop(seekBar.getProgress());
            return true;
        }
        return false;
      }
    });

    return v;
  }

  private void moveForward() {
    String command = CmdTextBuilder.build('c', "setMotorMotion", 1, 1, 2, 1);
    sendCommand(command);
  }

  private void moveBackward() {
    String command = CmdTextBuilder.build('c', "setMotorMotion", 1, 0, 2, 0);
    sendCommand(command);
  }

  private void moveStop() {
    String command = CmdTextBuilder.build('c', "setMotorMotion", 1, 2, 2, 2);
    sendCommand(command);
  }

  private void rotateLeft(int progress) {
    int speed = lerp(progress, 0, 100, 0, 255);
    int leftSpeed   = (int)(speed * (1 - SPEED_DIFF_FACTOR));
    int rightSpeed  = (int)(speed * (1 + SPEED_DIFF_FACTOR));

    String command = CmdTextBuilder.build('c', "setMotorSpeed", 1, leftSpeed, 2, rightSpeed);
    sendCommand(command);
  }

  private void rotateRight(int progress) {
    int speed = lerp(progress, 0, 100, 0, 255);
    int leftSpeed   = (int)(speed * (1 + SPEED_DIFF_FACTOR));
    int rightSpeed  = (int)(speed * (1 - SPEED_DIFF_FACTOR));

    String command = CmdTextBuilder.build('c', "setMotorSpeed", 1, leftSpeed, 2, rightSpeed);
    sendCommand(command);
  }

  private void rotateStop(int progress) {
    int speed = lerp(progress, 0, 100, 0, 255);

    String command = CmdTextBuilder.build('c', "setMotorSpeed", 1, speed, 2, speed);
    sendCommand(command);
  }

  /*
   *    (intput - minInput)/(maxInput - minInput) == (output - minOutput)/(maxOuput - minOutput)
   * => output = (input - minInput) / (maxInput - minInput) * (maxOutput - minOutput) + minOutput
   */
  private int lerp(double input, double minInput, double maxInput, double minOutput, double maxOutput) {
    double output = (input - minInput) / (maxInput - minInput) * (maxOutput - minOutput) + minOutput;
    return (int)output;
  }

  private void sendCommand(String command) {
    ((MainActivity)getActivity()).sendCommand(command);
  }
}

