package com.weirong.suntoy.ctrlh;

import java.util.ArrayList;
import java.util.StringTokenizer;

// type;name[;arg1[&arg2][&arg3]...]
public class CmdTextBuilder {
  private char    _type = ' ';
  private String  _name = "";
  private String  _args = "";
  private ArrayList<String> _argList = null;

  public static String build(char type, String name, String[] args) {
    StringBuilder sb = new StringBuilder();
    sb.append(type).append(';').append(name);
    if (args != null) {
      for (int i = 0; i < args.length; i++) {
        if (i == 0) sb.append(';');
        else sb.append('&');
        sb.append(args[i]);
      }
    }
    return sb.toString();
  }

  public static String build(char type, String name, int ...args) {
    StringBuilder sb = new StringBuilder();
    sb.append(type).append(';').append(name);
    if (args != null) {
      for (int i = 0; i < args.length; i++) {
        if (i == 0) sb.append(';');
        else sb.append('&');
        sb.append(args[i]);
      }
    }
    return sb.toString();
  }

  public static String build(char type, String name) {
    StringBuilder sb = new StringBuilder();
    sb.append(type).append(';').append(name);
    return sb.toString();
  }

  public boolean parse(String data) {
    StringTokenizer st = new StringTokenizer(data, " ");
    if (st.hasMoreTokens()) _type = st.nextToken().charAt(0);
    else return false;

    if (st.hasMoreTokens()) _name = st.nextToken();
    else return false;

    // the remainders are args
    if (st.hasMoreTokens()) {
      _args = st.nextToken();
      _argList = new ArrayList<String>();
      StringTokenizer st2 = new StringTokenizer(_args, "&");
      while (st2.hasMoreTokens()) {
        _argList.add(st2.nextToken());
      }
    }

    return true;
  }

  public int howManyArg() {
    if (_argList != null)
      return _argList.size();
    else
      return 0;
  }

  public String getArgAt(int index) {
    if (_argList == null) throw new IndexOutOfBoundsException();
    else {
      return _argList.get(index);
    }
  }

  public static boolean parseOneKwArg(String kwarg, String key, String value) {
    StringTokenizer st = new StringTokenizer(kwarg, "=");
    if (st.hasMoreTokens()) key = st.nextToken();
    else return false;
    if (st.hasMoreTokens()) value = st.nextToken();
    else return false;

    if (st.hasMoreTokens()) return false;
    return true;
  }

}
