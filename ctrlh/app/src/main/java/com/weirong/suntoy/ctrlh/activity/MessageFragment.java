package com.weirong.suntoy.ctrlh.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.weirong.suntoy.ctrlh.R;

public class MessageFragment extends Fragment {
  private static final String TAG = "MessageFragment";
  private TextView _textView = null;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_message, container, false);
    _textView = (TextView)v.findViewById(R.id.log_text_view);
    return v;
  }

  public void addText(String text) {
    _textView.append(text);
  }

  public void clearText() {
    _textView.setText("");
  }

}
