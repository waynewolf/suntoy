package com.weirong.suntoy.ctrlh.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.weirong.suntoy.ctrlh.ReceivingTextAssembler;
import com.weirong.suntoy.ctrlh.TextProtoCallback;
import com.weirong.suntoy.ctrlh.service.Constants;
import com.weirong.suntoy.ctrlh.R;
import com.weirong.suntoy.ctrlh.service.BtSerialService;

public abstract class BaseActivity extends FragmentActivity implements TextProtoCallback {
  private static final String TAG = "BaseActivity";
  private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
  private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
  private static final int REQUEST_ENABLE_BT = 3;

  private FragmentActivity _thisActivity = null;

  private BluetoothAdapter _btAdapter = null;
  private BtSerialService _btSerialService = null;
  private String _connectedDeviceName = null;
  private ReceivingTextAssembler _textAssembler = null;

  protected abstract void clearMessage();
  protected abstract void addMessage(String text);

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    _thisActivity = this;
    _textAssembler = new ReceivingTextAssembler(this);

    _btAdapter = BluetoothAdapter.getDefaultAdapter();
    if (_btAdapter == null) {
      Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
      this.finish();
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    // request enable BT if not on,
    // newBtSerialService() will then be called during onActivityResult
    if (!_btAdapter.isEnabled()) {
      Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
      startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
    } else if (_btSerialService == null) {
      newBtSerialService();
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    stopBtSerialService();
  }

  @Override
  public void onResume() {
    super.onResume();

    // Performing this check in onResume() covers the case in which BT was
    // not enabled during onStart(), so we were paused to enable it...
    // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
    startBtSerialService();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.action_bar, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
//      case R.id.secure_connect_scan: {
//        Intent serverIntent = new Intent(this, DeviceListActivity.class);
//        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
//        return true;
//      }
      case R.id.insecure_connect_scan: {
        Intent serverIntent = new Intent(this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
        return true;
      }
      case R.id.discoverable: {
        ensureDiscoverable();
        return true;
      }
    }
    return false;
  }

  private void ensureDiscoverable() {
    if (_btAdapter.getScanMode() !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
      Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
      discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
      startActivity(discoverableIntent);
    }
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    switch (requestCode) {
      case REQUEST_CONNECT_DEVICE_SECURE:
        // DeviceListActivity returns with a device to connect
        if (resultCode == Activity.RESULT_OK) {
          connectDevice(data, true);
        }
        break;
      case REQUEST_CONNECT_DEVICE_INSECURE:
        // DeviceListActivity returns with a device to connect
        if (resultCode == Activity.RESULT_OK) {
          connectDevice(data, false);
        }
        break;
      case REQUEST_ENABLE_BT:
        // the request to enable Bluetooth returns
        if (resultCode == Activity.RESULT_OK) {
          // Bluetooth is now enabled, so start BtSerialService
          newBtSerialService();
        } else {
          // User did not enable Bluetooth or an error occurred
          Toast.makeText(this, R.string.bt_not_enabled_leaving,
                  Toast.LENGTH_SHORT).show();
          this.finish();
        }
    }
  }

  private void newBtSerialService() {
    _btSerialService = new BtSerialService(this, mHandler);
  }

  public void startBtSerialService() {
    if (_btSerialService != null) {
      if (_btSerialService.getState() == BtSerialService.STATE_NONE) {
        _btSerialService.start();
      }
    }
  }

  public void stopBtSerialService() {
    if (_btSerialService != null) {
      _btSerialService.stop();
    }
  }

  private final Handler mHandler = new Handler() {
    @Override
    public void handleMessage(Message msg) {
      switch (msg.what) {
        case Constants.MESSAGE_STATE_CHANGE:
          switch (msg.arg1) {
            case BtSerialService.STATE_CONNECTED:
              setStatus(getString(R.string.title_connected_to, _connectedDeviceName));
              clearMessage();
              break;
            case BtSerialService.STATE_CONNECTING:
              setStatus(R.string.title_connecting);
              break;
            case BtSerialService.STATE_LISTEN:
            case BtSerialService.STATE_NONE:
              setStatus(R.string.title_not_connected);
              break;
          }
          break;
        case Constants.MESSAGE_WRITE:
          byte[] writeBuf = (byte[]) msg.obj;
          String writeMessage = new String(writeBuf);
          addMessage(writeMessage);
          break;
        case Constants.MESSAGE_READ:
          byte[] readBuf = (byte[]) msg.obj;
          String readMessage = new String(readBuf, 0, msg.arg1);
          Log.i(TAG, "got: " + readMessage);
          _textAssembler.addText(readMessage);
          break;
        case Constants.MESSAGE_DEVICE_NAME:
          _connectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
          if (null != _thisActivity) {
            Toast.makeText(_thisActivity, "Connected to "
                    + _connectedDeviceName, Toast.LENGTH_SHORT).show();
          }
          break;
        case Constants.MESSAGE_TOAST:
          if (null != _thisActivity) {
            Toast.makeText(_thisActivity, msg.getData().getString(Constants.TOAST),
                    Toast.LENGTH_SHORT).show();
          }
          break;
      }
    }
  };

  private void setStatus(int resId) {
    if (null == _thisActivity) {
      return;
    }
    final ActionBar actionBar = _thisActivity.getActionBar();
    if (null == actionBar) {
      return;
    }
    actionBar.setSubtitle(resId);
  }

  private void setStatus(CharSequence subTitle) {
    if (null == _thisActivity) {
      return;
    }
    final ActionBar actionBar = _thisActivity.getActionBar();
    if (null == actionBar) {
      return;
    }
    actionBar.setSubtitle(subTitle);
  }

  private void connectDevice(Intent data, boolean secure) {
    String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
    BluetoothDevice device = _btAdapter.getRemoteDevice(address);
    _btSerialService.connect(device, secure);
  }

  public void sendCommand(String command) {
    // Check that we're actually connected before trying anything
    if (_btSerialService.getState() != BtSerialService.STATE_CONNECTED) {
      Toast.makeText(_thisActivity, R.string.not_connected, Toast.LENGTH_SHORT).show();
      return;
    }

    // Check that there's actually something to send
    if (command.length() > 0) {
      if (!command.endsWith("\n")) command += "\n";

      byte[] send = command.getBytes();
      _btSerialService.write(send);
    }
  }

  public void onReceiveReply(String reply) {
    int errCode = ReceivingTextAssembler.getReplyErrorCode(reply);
    String optData = ReceivingTextAssembler.getReplyOptionalData(reply);
    if (errCode < 0) {
      //TODO
      ;
    }
    addMessage(reply);
  }
}