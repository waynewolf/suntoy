package com.weirong.suntoy.ctrlh;

public interface TextProtoCallback {
  void onReceiveReply(String reply);
}
