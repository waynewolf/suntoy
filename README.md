# suntoy

Toy for my sons.


## development enviornment

HW

1. hikey960 dev board

2. 16-way pwm with I2C on hikey960 LS 40pin I2C1

3. Arduino UNO R3

4. Adafruit Motor Shield V2

5. HC-05 BT module

6. HC-SR04 UltraSonic distancing module


SW

1. android studio 2.3.2 on ubuntu 16.04 64bit host

2. android O preview on target device

3. kernel source from wget https://dl.google.com/dl/android/aosp/arm-hikey960-NOU-7ad3cccc.tgz, refer to https://source.android.com/source/devices

4. arm-linux-gnueabihf- linaro-1.13.1-4.9-2014.07

5. aarch64-linux-gnu- linaro-1.13.1-4.9-2014.07


## Pin Map

1. Right Wheel -> M2 of Adafruit Motor Shield v2
2. Left Wheel -> M1 of Adafruit Motor Shield v2
3. HC-05 RXD -> Arduino Uno D6
4. HC-05 TXD -> Arduino Uno D5
5. HC-05 VCC -> Arduino Uno 5v
6. HC-05 GND -> Arduino Uno GND

## Photo

![RC BT Car](photo/1.jpg)
![RC BT Car](photo/2.png)

## reference
1. hikey960 Low speed 40pin gpio pin and linux gpio # map:  
https://docs.google.com/spreadsheets/d/1QVfI1AaRO_7IhSM3HMSSj1HewlzR2BxVlQXW7sU54K8/edit#gid=0

2. adafruit motor shield v2:  
https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino

3. HC-05 datasheet:  
http://www.electronicaestudio.com/docs/istd016A.pdf
