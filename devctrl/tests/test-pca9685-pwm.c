/*
 * This file test pca9685 interface through /sys/class/pwm/pwmchip0/xxx
 *
 * rely on kernel CONFIG_PWM_PCA9685
 */
#include <dirent.h>
#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "os_util.h"

#define PWM_ROOT_PATH "/sys/class/pwm/pwmchip0"

/*
 * list pwm devices that have been exported
 */
void list_pwm() {
  DIR *d;
  struct dirent *dir;

  int found = 0;
  char result[1024];

  d = opendir(PWM_ROOT_PATH);
  if (d) {
    while ((dir = readdir(d)) != NULL) {
      struct stat statbuf;
      char path[1024];
      snprintf(path, 1023, "%s/%s", PWM_ROOT_PATH, dir->d_name);
      stat(path, &statbuf);
      if (S_ISDIR(statbuf.st_mode)) {
        /* the directory name begins with pwm */
        if (strstr(dir->d_name, "pwm") == dir->d_name &&
            strlen(dir->d_name) > 3) {
          char *channel = dir->d_name + 3;
          char line[16];
          snprintf(line, 15, "channel: %s\n", channel);
          strcat(result, line);
          found = 1;
        }
      }
    }
    closedir(d);
  }

  if (found)
    printf("%s\n", result);
  else
    printf("No pwm enabled, try echo channel_number > %s/export\n",
           PWM_ROOT_PATH);
}

/*
 * request pwm channel
 *
 * @export_flag 1 to export, 0 to unexport
 */
void request_channel(int channel, int export_flag) {
  char path[64] = PWM_ROOT_PATH;

  if (export_flag)
    strcat(path, "/export");
  else
    strcat(path, "/unexport");

  if (!write_int_to_file(path, channel))
    printf("request_channel: channel=%d, export_flag=%d\n", channel,
           export_flag);
  else
    printf("request_channel failed\n");
}

/*
 * set pwm channel parameters,
 *
 * 0 on success, negative value for error
 */
int set_channel_params(int channel, int period, int duty_cycle) {
  printf("%s: channel=%d, period=%d, duty-cycle=%d\n", __FUNCTION__, channel,
         period, duty_cycle);
  char path_enable[128], path_period[128], path_duty_cycle[128];

  snprintf(path_enable, 127, "%s/pwm%d/enable", PWM_ROOT_PATH, channel);
  snprintf(path_period, 127, "%s/pwm%d/period", PWM_ROOT_PATH, channel);
  snprintf(path_duty_cycle, 127, "%s/pwm%d/duty_cycle", PWM_ROOT_PATH, channel);

  int ret1 = 0, ret2 = 0, ret3 = 0;
  ret1 = write_int_to_file(path_enable, 1);
  if (ret1 >= 0 && period != -1)
    ret2 = write_int_to_file(path_period, period);
  if (duty_cycle != -1)
    ret3 = write_int_to_file(path_duty_cycle, duty_cycle);

  if (ret1 >= 0 && ret2 >= 0 && ret3 >= 0)
    return 0;
  else
    return -1;
}

/*
 * interactive session loop, type q|quit to exit
 */
void interactive_pwm_session(int channel) {
  printf("interactive_pwm_session for channel %d\n", channel);
  printf("format: period duty_cycle\n");
  printf(">> ");

  char user_input[1024];
  while (fgets(user_input, 1023, stdin)) {
    // fgets put newline into the buffer, remove it
    user_input[strlen(user_input) - 1] = '\0';
    if (!strcmp(user_input, "q") || !strcmp(user_input, "quit"))
      break;
    int period = -1, duty_cycle = -1;
    sscanf(user_input, "%d %d", &period, &duty_cycle);
    set_channel_params(channel, period, duty_cycle);
    printf("\n>> ");
  }
  printf("Quit interactive session\n");
}

void usage(char *argv[]) {
  fprintf(stderr, "Usage:\n");
  fprintf(stderr, "   %s --help|-h\n\t%s\n", argv[0], "show help");
  fprintf(stderr, "   %s -l\n\t%s\n", argv[0], "list enabled pwm devices");
  fprintf(stderr, "   %s -c channel -e\n\t%s\n", argv[0],
          "enable pwm channel <channel>");
  fprintf(stderr, "   %s -c channel -d\n\t%s\n", argv[0],
          "disable pwm channel <channel>");
  fprintf(stderr,
          "   %s -c channel --period period --duty-cycle duty-cycle\n\t%s\n",
          argv[0], "set pwm period and duty cycle of channel <channel>");
  fprintf(stderr, "   %s -c channel -i\n\t%s\n", argv[0],
          "set pwm period and duty cycle of channel <channel> interactively");
}

int main(int argc, char *argv[]) {
  int c;
  int digit_optind         = 0;
  int do_list_pwm          = 0;
  int pwm_channel          = -1;
  int do_request_channel   = 0;
  int do_unrequest_channel = 0;
  int show_help            = 0;
  int interactive          = 0;

  int period     = -1;
  int duty_cycle = -1;

  while (1) {
    int this_option_optind              = optind ? optind : 1;
    int option_index                    = 0;
    static struct option long_options[] = {
        {"help", no_argument, 0, 'h'},
        {"period", required_argument, 0, 'p'},
        {"duty-cycle", required_argument, 0, 'y'},
        {0, 0, 0, 0}};

    c = getopt_long(argc, argv, "hlc:edi", long_options, &option_index);
    if (c == -1)
      break;

    switch (c) {
    case 'h':
      show_help = 1;
      break;

    case 'l':
      do_list_pwm = 1;
      break;

    case 'c':
      pwm_channel = atoi(optarg);
      break;

    case 'e':
      if (pwm_channel == -1) {
        fprintf(stderr,
                "the argument -e is useful only when -c <channel> exists\n");
        usage(argv);
        return EXIT_FAILURE;
      }
      do_request_channel = 1;
      break;

    case 'd':
      if (pwm_channel == -1) {
        fprintf(stderr,
                "the argument -d is useful only when -c <channel> exists\n");
        usage(argv);
        return EXIT_FAILURE;
      }
      do_unrequest_channel = 1;
      break;

    case 'p':
      if (pwm_channel == -1) {
        fprintf(
            stderr,
            "the argument --period is useful only when -c <channel> exists\n");
        usage(argv);
        return EXIT_FAILURE;
      }
      period = atoi(optarg);
      break;

    case 'y':
      if (pwm_channel == -1) {
        fprintf(stderr, "the argument --duty-cycle is useful only when -c "
                        "<channel> exists\n");
        usage(argv);
        return EXIT_FAILURE;
      }
      duty_cycle = atoi(optarg);
      break;

    case 'i':
      if (pwm_channel == -1) {
        fprintf(stderr,
                "the argument -i is useful only when -c <channel> exists\n");
        usage(argv);
        return EXIT_FAILURE;
      }
      interactive = 1;
      break;

    default:
      fprintf(stderr, "getopt returned character code 0%o ??\n", c);
      usage(argv);
      return EXIT_FAILURE;
    }
  }

  if (optind < argc) {
    fprintf(stderr, "not recognized arguments: ");
    while (optind < argc)
      fprintf(stderr, "%s ", argv[optind++]);
    fprintf(stderr, "\n");
    usage(argv);
    return EXIT_FAILURE;
  }

  if (show_help && do_list_pwm) {
    fprintf(stderr, "-h and -l don't coexist\n");
    usage(argv);
    return EXIT_FAILURE;
  }

  if (show_help && pwm_channel != -1) {
    fprintf(stderr, "-h and -c don't coexist\n");
    usage(argv);
    return EXIT_FAILURE;
  }

  if (do_list_pwm && pwm_channel != -1) {
    fprintf(stderr, "-l and -c don't coexist\n");
    usage(argv);
    return EXIT_FAILURE;
  }

  if (pwm_channel != -1) {
    int request_or_unrequest_channel =
        do_request_channel || do_unrequest_channel;
    int period_or_duty_cycle_set = (period != -1 || duty_cycle != -1);
    if (request_or_unrequest_channel && period_or_duty_cycle_set) {
      fprintf(stderr, "you cannot enable/disable channel, and set "
                      "period/duty-cycle together\n");
      usage(argv);
      return EXIT_FAILURE;
    }
    if (request_or_unrequest_channel && interactive) {
      fprintf(stderr, "you cannot enable/disable channel, and start an "
                      "interactive session\n");
      usage(argv);
      return EXIT_FAILURE;
    }
    if (period_or_duty_cycle_set && interactive) {
      fprintf(stderr, "you cannot start an interactive session, and set "
                      "period/duty-cycle at the same time\n");
      usage(argv);
      return EXIT_FAILURE;
    }

    if (!period_or_duty_cycle_set && !interactive &&
        !request_or_unrequest_channel) {
      fprintf(stderr, "what do you want to do with the channel: %d ?\n",
              pwm_channel);
      usage(argv);
      return EXIT_FAILURE;
    }

    if (request_or_unrequest_channel) {
      if (do_request_channel)
        request_channel(pwm_channel, 1);
      if (do_unrequest_channel)
        request_channel(pwm_channel, 0);
    }

    if (interactive) {
      interactive_pwm_session(pwm_channel);
    }

    if (period_or_duty_cycle_set) {
      set_channel_params(pwm_channel, period, duty_cycle);
    }
  }

  if (show_help) {
    usage(argv);
    return EXIT_SUCCESS;
  }

  if (do_list_pwm)
    list_pwm();

  return EXIT_SUCCESS;
}
