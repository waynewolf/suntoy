#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

void usage(char *argv[]) {
  fprintf(stderr, "Usage: %s -b bus -a address [-r bytes] [-w value]\n",
          argv[0]);
}

int main(int argc, char *argv[]) {
  int bus          = -1;
  int address      = -1;
  int read_n_bytes = -1;
  int write_value  = -1;

  int opt;
  while ((opt = getopt(argc, argv, "b:a:r:w:")) != -1) {
    switch (opt) {
    case 'b':
      bus = atoi(optarg);
      break;
    case 'a':
      address = atoi(optarg);
      break;
    case 'r':
      read_n_bytes = atoi(optarg);
      break;
    case 'w':
      write_value = atoi(optarg);
      break;
    default: /* '?' */
      usage(argv);
      return -1;
    }
  }

  // mandatory arguments
  if (bus == -1 || address == -1) {
    usage(argv);
    return -1;
  }

  char i2c_name[20];
  snprintf(i2c_name, 19, "/dev/i2c-%d", bus);
  int file;
  if ((file = open(i2c_name, O_RDWR)) < 0) {
    fprintf(stderr, "open %s: %s\n", i2c_name, strerror(errno));
    return -1;
  }

  if (ioctl(file, I2C_SLAVE, address) < 0) {
    fprintf(stderr, "acquire bus access and/or talk to slave: %s\n",
            strerror(errno));
    return -1;
  }

  if (read_n_bytes != -1) {
    char *buf     = (char *)malloc(read_n_bytes);
    ssize_t nread = read(file, buf, read_n_bytes);
    if (nread < 0) {
      fprintf(stderr, "read %d byte(s) from i2c: %s\n", read_n_bytes,
              strerror(errno));
    } else {
      for (int i = 0; i < nread; ++i) {
        printf("%2d ", buf[i]);
      }
    }
    printf("\n");
    free(buf);
  }

  if (write_value != -1) {
    char buf = (char)write_value;
    if (write(file, &buf, 1) != 1) {
      fprintf(stderr, "write to the i2c bus: %s\n", strerror(errno));
    } else {
      printf("Write %d to i2c successful\n", (int)buf);
    }
  }

  close(file);

  return 0;
}