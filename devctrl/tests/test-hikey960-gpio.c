#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "os_util.h"

#define GPIO_DIRECTION "/sys/class/gpio/gpio296/direction"
#define GPIO_VALUE "/sys/class/gpio/gpio296/value"

void usage(char *argv[]) {
  fprintf(stderr, "Usage: %s -p periods_ns -d duty_cycle_ns\n", argv[0]);
}

int main(int argc, char *argv[]) {
  int opt;
  int periods    = -1;
  int duty_cycle = -1;

  while ((opt = getopt(argc, argv, "p:d:")) != -1) {
    switch (opt) {
    case 'p':
      periods = atoi(optarg);
      break;
    case 'd':
      duty_cycle = atoi(optarg);
      break;
    default: /* '?' */
      usage(argv);
      return -1;
    }
  }

  if (periods == -1 || duty_cycle == -1) {
    fprintf(
        stderr,
        "periods and/or duty_cycle arguments not set, they are mandatory\n");
    usage(argv);
    return -1;
  }

  int low_time = periods - duty_cycle;

  printf("gpio frequency: %0.3fkHz\n", 1000000.f / periods);

  if (write_str_to_file(GPIO_DIRECTION, "out") < 0) {
    fprintf(stderr, "%s write fail\n", GPIO_DIRECTION);
    return -1;
  }

  struct timespec req;
  req.tv_sec = 0;
  while (1) {
    write_int_to_file(GPIO_VALUE, 1);
    req.tv_nsec = duty_cycle;
    nanosleep(&req, NULL);
    write_int_to_file(GPIO_VALUE, 0);
    req.tv_nsec = low_time;
    nanosleep(&req, NULL);
  }
  return 0;
}