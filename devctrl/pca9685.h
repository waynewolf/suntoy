#ifndef PCA9685_H
#define PCA9685_H

#define PWM_CHIP_PATH "/sys/class/pwm/pwmchip0"

struct pca9685_pwm {
  int channel;
};

#ifdef __cplusplus
extern "C" {
#endif

struct pca9685_pwm *pca9685_pwm_init(int channel);
void pca9685_pwm_term(struct pca9685_pwm *dev);

int pca9685_pwm_enable(struct pca9685_pwm *dev,
                       int flag /* 1 enable, 0 disable */);
int pca9685_pwm_set_freq(struct pca9685_pwm *dev, int freq /* in HZ */);
int pca9685_pwm_get_freq(struct pca9685_pwm *dev);

int pca9685_pwm_set_period_and_duty_cycle(struct pca9685_pwm *dev, int period,
                                          int duty_cycle);

int pca9685_pwm_set(struct pca9685_pwm *dev, const char *name, int value);
int pca9685_pwm_get(struct pca9685_pwm *dev, const char *name);

#ifdef __cplusplus
}
#endif

#endif