#ifndef OS_UTIL_H
#define OS_UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 * check if the file exists
 *
 * Attention! Check only the NORMAL file
 */
int file_exists(const char *file);

/*
 * check if the folder exists
 */
int dir_exists(const char *dir);

/*
 * write int to file
 *
 * return 0 for success, -1 for error
 */
int write_int_to_file(const char *file, int value);

/*
 * read int from file
 *
 * return 0 for success, -1 for error
 */
int read_int_from_file(const char *file, int *value);

/*
 * write null-terminated string to file
 *
 * return 0 for success, -1 for error
 */
int write_str_to_file(const char *file, const char *str);

#ifdef __cplusplus
}
#endif

#endif