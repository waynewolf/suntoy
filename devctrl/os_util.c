#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <getopt.h>
#include "os_util.h"

int file_exists(const char *file) {
  DIR *dirptr;

  if (access(file, F_OK) != -1) {
    if ((dirptr = opendir(file)) != NULL) {
      closedir(dirptr);
      return 0; /* file exists, but it's a dir */
    } else {
      return 1; /* file exists, and it's NORMAL file */
    }

  } else {
    return 0;
  }
}

int dir_exists(const char *dir) {
  DIR *d = opendir(dir);
  if (d) {
    /* Directory exists. */
    closedir(d);
    return 1;
  } else if (ENOENT == errno) {
    /* Directory does not exist. */
    return 0;
  } else {
    /* opendir() failed for some other reason. */
    fprintf(stderr, "opendir: %s\n", strerror(errno));
    return 0;
  }
}

int write_int_to_file(const char *file, int value) {
  if (!file_exists(file))
    return -1;
  FILE *fp = fopen(file, "w");
  if (!fp) {
    fprintf(stderr, "open file %s: %s\n", file, strerror(errno));
    return -1;
  }
  char value_string[16];
  snprintf(value_string, 15, "%d", value);
  fwrite(value_string, 1, strlen(value_string), fp);
  fclose(fp);

  return 0;
}

int read_int_from_file(const char *file, int *value) {
  FILE *fp = fopen(file, "r");
  if (!fp) {
    fprintf(stderr, "open file %s: %s\n", file, strerror(errno));
    return -1;
  }
  fread(value, 1, sizeof(int), fp);
  fclose(fp);

  return 0;
}

int write_str_to_file(const char *file, const char *str) {
  if (!file || !str || !file_exists(file))
    return -1;

  FILE *fp = fopen(file, "w");
  if (!fp) {
    fprintf(stderr, "open file %s: %s\n", file, strerror(errno));
    return -1;
  }
  fwrite(str, 1, strlen(str), fp);
  fclose(fp);

  return 0;
}