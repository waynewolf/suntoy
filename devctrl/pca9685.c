#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <unistd.h>
#include "pca9685.h"
#include "os_util.h"

struct pca9685_pwm *pca9685_pwm_init(int channel) {
  struct pca9685_pwm *dev =
      (struct pca9685_pwm *)malloc(sizeof(struct pca9685_pwm));
  if (!dev) {
    fprintf(stderr, "allocate memory of struct pca9685_pwm: %s\n",
            strerror(errno));
    return NULL;
  }

  // if this channel is not exported, request the channel like "echo 0 > export"
  char path[64];
  snprintf(path, 64, "%s/pwm%d", PWM_CHIP_PATH, channel);
  if (!file_exists(path)) {
    snprintf(path, 63, "%s/export", PWM_CHIP_PATH);

    if (write_int_to_file(path, channel) >= 0) {
      sleep(1);
      snprintf(path, 63, "%s/pwm%d", PWM_CHIP_PATH, channel);
      if (!dir_exists(path)) {
        fprintf(stderr, "kernel failed to popup pwm folder %s\n", path);
        free(dev);
        return NULL;
      }
    }
  }

  dev->channel = channel;

  return dev;
}

void pca9685_pwm_term(struct pca9685_pwm *dev) {
  // if the channel is exported, close the channel like "echo 0 > unexport"
  char path[64];
  snprintf(path, 64, "%s/unexport", PWM_CHIP_PATH);
  write_int_to_file(path, dev->channel);
  if (dev)
    free(dev);
}

int pca9685_pwm_enable(struct pca9685_pwm *dev,
                       int flag /* 1 enable, 0 disable */) {
  char path[64];
  if (flag == 1)
    snprintf(path, 64, "%s/export", PWM_CHIP_PATH);
  if (flag == 0)
    snprintf(path, 64, "%s/unexport", PWM_CHIP_PATH);

  return write_int_to_file(path, dev->channel);
}

int pca9685_pwm_set_freq(struct pca9685_pwm *dev, int freq /* in HZ */) {
  // TODO:
  dev  = dev;
  freq = freq;

  return 0;
}

int pca9685_pwm_get_freq(struct pca9685_pwm *dev) {
  // TODO:
  dev = dev;
  return 0;
}

int pca9685_pwm_set(struct pca9685_pwm *dev, const char *name, int value) {
  int ret = 0;
  char path[64];
  snprintf(path, 64, "%s/pwm%d/%s", PWM_CHIP_PATH, dev->channel, name);

  if (file_exists(path)) {
    if (write_int_to_file(path, value) < 0) {
      fprintf(stderr, "write %s: %s\n", path, strerror(errno));
      ret = -1;
    }
  }

  return ret;
}

int pca9685_pwm_get(struct pca9685_pwm *dev, const char *name) {
  int ret = 0;
  char path[64];
  snprintf(path, 64, "%s/pwm%d/%s", PWM_CHIP_PATH, dev->channel, name);

  if (file_exists(path)) {
    if (read_int_from_file(path, &ret) < 0) {
      fprintf(stderr, "read %s: %s\n", path, strerror(errno));
      ret = -1;
    }
  }

  return ret;
}

int pca9685_pwm_set_period_and_duty_cycle(struct pca9685_pwm *dev, int period,
                                          int duty_cycle) {
  int ret1 = 0, ret2 = 0;
  if (period >= 0)
    ret1 = pca9685_pwm_set(dev, "period", period);
  if (duty_cycle >= 0)
    ret2 = pca9685_pwm_set(dev, "duty_cycle", duty_cycle);

  if (ret1 >= 0 && ret2 >= 0)
    return 0;
  else
    return -1;
}

#if 0

void main() {
	struct pca9685_pwm* pwm = pca9685_pwm_init(0);

	pca9685_pwm_set_period_and_duty_cycle(pwm, 1000000, 500000);
	pca9685_pwm_enable(pwm, 1);

	pca9685_pwm_term(pwm);
}

#endif